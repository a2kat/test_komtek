﻿using SQLite.CodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace WebPatient.DAL
{
    public class PatientsContext : DbContext
    {
        public PatientsContext()
           : base("PatientsContext")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            var sqliteConnectionInitializer = new SqliteDropCreateDatabaseWhenModelChanges<PatientsContext>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);
        }
        public DbSet<WebPatient.Models.Patient> Patients { get; set; }
        public DbSet<WebPatient.Models.Measurement> Measurements { get; set; }

    }
}