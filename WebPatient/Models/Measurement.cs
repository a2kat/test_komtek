﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebPatient.Models
{
    public class Measurement
    {
        public Measurement()
        {

        }
        public Measurement(long PatientID)
        {
            this.PatientID = PatientID;
            Patient = new WebPatient.DAL.PatientsContext().Patients.FirstOrDefault(x => x.PatientID == this.PatientID);
        }
        public long PatientID { get; set; }
        public long MeasurementID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата добавления")]
        [Required(ErrorMessage = "Дата добавления обязательна")]
        public DateTime AddingDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата измерения")]
        public DateTime MeasuringDate { get; set; }

        [Display(Name = "Пульс")]
        [Range(55, 170.00,
            ErrorMessage = "Пульс должен быть в впределах [55 - 170]")]
        public byte ?Pulse { get; set; }

        [Display(Name = "Диастолическое давление")]
        [Range(40, 140.00,
            ErrorMessage = "Диастолическое давление должно быть в впределах [40 - 140]")]
        [Required(ErrorMessage = "Диастолическое давление обязательно")]
        public byte DiastolicPressure { get; set; }

        [Display(Name = "Систолическое давление")]
        [Range(60, 200.00,
            ErrorMessage = "Систолическое давление должно быть в впределах [60 - 200]")]
        [Required(ErrorMessage = "Систолическое давление обязательно")]
        public byte SystolicPressure { get; set; }

        public virtual Patient Patient { get; set; }

    }
}