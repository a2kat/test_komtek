﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Globalization;

namespace WebPatient.Models
{
    public class Patient
    {
        public long PatientID { get; set; }

        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Фамилия обязательна")]
        [StringLength(160)]
        public string LastName { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Имя обязательно")]
        [StringLength(160)]
        public string FirstName { get; set; }

        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        /*
        [Display(Name = "СНИЛС")]
        [SnilsMask("###-###-###-##",ErrorMessage = "{0} задается согласно шаблону {1}.")]
        public string Snils { get; set; }
        */
        [Display(Name = "СНИЛС")]
        [Required(ErrorMessage = "Снилс")]
        [RegularExpression("^\\d{3}-\\d{3}-\\d{3}-\\d{2}$", ErrorMessage = "{0} задается согласно шаблону ###-###-###-##")]
        public string Snils { get; set; }

        [Display(Name = "Пол")]
        [Required(ErrorMessage = "Пол обязателен")]
        public Gender Gender { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Дата рождения")]
        [Required(ErrorMessage = "Дата рождения обязательна")]
        public DateTime DateOfBirth { get; set; }

        public virtual ICollection<Measurement> Measurements { get; set; }
    }
    public enum Gender
    {
        Муж, Жен
    }


    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    sealed public class SnilsMaskAttribute : ValidationAttribute
    {
        readonly string _mask;

        public string Mask
        {
            get { return _mask; }
        }

        public SnilsMaskAttribute(string mask)
        {
            _mask = mask;
        }


        public override bool IsValid(object value)
        {
            var snilsNumber = (String)value;
            bool result = true;
            if (this.Mask != null)
            {
                result = MatchesMask(this.Mask, snilsNumber);
            }
            return result;
        }

        internal bool MatchesMask(string mask, string snilsNumber)
        {

            if (mask.Length != snilsNumber?.Trim().Length)
            {
                return false;
            }
            for (int i = 0; i < mask.Length; i++)
            {
                if (mask[i] == 'd' && char.IsDigit(snilsNumber[i]) == false)
                {
                    return false;
                }
                if (mask[i] == '-' && snilsNumber[i] != '-')
                {
                    return false;
                }
            }
            return true;
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture,
              ErrorMessageString, name, this.Mask);
        }

    }
}

