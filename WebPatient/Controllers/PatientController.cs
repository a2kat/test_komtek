﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPatient.DAL;
using WebPatient.Models;

namespace WebPatient.Controllers
{
    public class PatientController : Controller
    {
        private PatientsContext context;

        public PatientController()
        {
            this.context = new PatientsContext();
        }

        [HttpGet]
        public ActionResult EditPatientModal(long PatientID)
        {
            return PartialView("_EditPatientsModal", context.Patients.Where(p => p.PatientID == PatientID).FirstOrDefault());
        }

        [HttpGet]
        public ActionResult AddPatient()
        {
            return View("AddPatient", new Patient());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPatient(Patient model)
        {
            if (ModelState.IsValid)
            {
                using (var db = this.context)
                {
                    db.Patients.Add(model);
                    db.SaveChanges();
                }

                return RedirectToActionPermanent("Index", "Home");
            }

            ModelState.AddModelError("name", "");

            return View("AddPatient", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdatePatient(Patient model)
        {
            if (ModelState.IsValid)
            {
                using (var db = this.context)
                {

                    var result = db.Patients.SingleOrDefault(p => p.PatientID == model.PatientID);
                    if (result != null)
                    {
                        db.Entry(result).CurrentValues.SetValues(model);
                        db.SaveChanges();
                    }
                }
                return Content("{\"success\": \"Пациент успешно сохранен\"}", "application/json");
            }

            ModelState.AddModelError("name", "");
            return PartialView("_GetPatientsModal", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePatient(Patient model)
        {
            try
            {
                using (var db = this.context)
                {
                    var result = db.Patients.SingleOrDefault(p => p.PatientID == model.PatientID);
                    if (result != null)
                    {
                        db.Patients.Remove(result);
                        db.SaveChanges();
                        return Content("{\"success\": \"Пациент успешно удален\"}", "application/json");
                    }
                    return Content("{\"error\": \"Пациент в базе не найден\"}", "application/json");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"error\": \"Пациент не удален " + ex.Message + "\"}", "application/json");
            }

        }
    }
}