﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPatient.DAL;
using WebPatient.Models;

namespace WebPatient.Controllers
{
    public class HomeController : Controller
    {
        private PatientsContext context;

        public HomeController()
        {
            this.context = new PatientsContext();
        }

        public ActionResult Index()
        {
            return View(context.Patients.ToList());
        }

        [HttpGet]
        public ActionResult Measurements(long PatientID)
        {
            ViewBag.Measurements = context.Patients.Where(p => p.PatientID == PatientID).FirstOrDefault().Measurements;
            ViewBag.Patient = context.Patients.FirstOrDefault(x => x.PatientID == PatientID);
            return View();
        }

        [HttpGet]
        public ActionResult AddMeasurementModal(long PatientID)
        {
            return PartialView("_GetMeasurementModal", new Measurement(PatientID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddMeasurement(Measurement model)
        {
            if (model.DiastolicPressure > model.SystolicPressure)
            {
                ModelState.AddModelError("SystolicPressure", "Систолическое давление должно быть больше Диастолического давления");
            }
   
            if (ModelState.IsValid)
            {
                using (var db = this.context)
                {
                    model.AddingDate = DateTime.Now;
                    db.Measurements.Add(model);
                    db.SaveChanges();
                }
                return Content("{\"success\": \"Запись успешно добавлена\"}", "application/json"); 
            } else
            {
                ModelState.AddModelError("name", "");
            }

            
            return PartialView("_GetMeasurementModal", model);
        }


        [HttpPost]
        public ActionResult DeleteMeasure(long MeasurementID)
        {
            try
            {
                using (var db = this.context)
                {
                    var result = db.Measurements.SingleOrDefault(m => m.MeasurementID == MeasurementID);
                    if (result != null)
                    {
                        db.Measurements.Remove(result);
                        db.SaveChanges();
                        return Content("{\"success\": \"Измерение успешно удалено\"}", "application/json");
                    }
                    return Content("{\"error\": \"Измерение в базе не найдено\"}", "application/json");
                }
            }
            catch (Exception ex)
            {
                return Content("{\"error\": \"Измерение не удалено " + ex.Message + "\"}", "application/json");
            }

        }
    }

  
}